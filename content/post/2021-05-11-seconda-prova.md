---
title: Seconda Prova 
date: 2021-05-11
---
{{< youtube id="v3dPhA8gs-Mw" >}}

## Questo è il video della seconda prova caricato su Youtube

NAO, oltre a essere un umanoide molto simpatico, è anche in grado di risolvere vari problemi. Nella nostra seconda prova dimostriamo come NAO, grazie all’aiuto del dispositivo esterno mattoncino di tipo EV3, sia in grado di parlare numerose lingue in base a un input visivo riconosciuto dai sensori di luce dell’EV3. 
Il problema che abbiamo identificato riguarda le persone di nazionalità differenti quando visitano luoghi o eventi al di fuori del loro paese. Non conoscendo bene la lingua di quella nazione, possono trovarsi in difficoltà nell’orientamento. La vasta conoscenza delle lingue di NAO permette di dargli una panoramica generale del luogo che stanno visitando, tutto con la loro lingua nativa! I turisti, per farsi riconoscere da NAO, dovranno mostrare un biglietto di un certo colore, così che il robot li riconosca (esempio: biglietto color rosso = nazionalità cinese).
