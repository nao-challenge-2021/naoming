---
title: Terza Prova 
date: 2021-05-12
---
{{< youtube id="Nl7m_n68CRk" >}}

## Questo è il video della terza prova caricato su Youtube

In onore del settecentenario dalla morte di Dante Alighieri, abbiamo deciso di proporre una rivisitazione in chiave moderna della sua Divina Commedia, più precisamente abbiamo stravolto la visita di Dante (NAO) nel girone dei superbi. In questo luogo di straziante agonia ci finiscono coloro che si sono presi tutti i meriti di un lavoro svolto, senza riconoscere la importanza delle macchine. La guida che accompagna Dante è Steve Jobs, e il guardiano di questo girone è un gigantesco Game Boy. La punizione che subiranno i dannati sarà un’eternità dove saranno costretti a lavorare in modo lento e sempre sbagliato, tutti sotto forma di macchine guaste.
Che aspettate? Andate a guardare il video della nostra terza prova della #Naochallenge!
