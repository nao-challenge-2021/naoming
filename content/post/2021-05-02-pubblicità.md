---
title: Pubblicità per NAO
date: 2021-05-02
---
{{< youtube id="Qh-cw4uM5Ms" >}}

## Questo è il video della pubblicità caricato su Youtube

L’umanoide NAO ritrova molteplici utilità in vari settori, soprattutto in quello della didattica. Grazie al suo sofisticato riconoscimento volti, alla sua capacità di parola, al suo ascolto e ai suoi movimenti realistici è possibile la comunicazione con studenti che sono interessati al mondo della robotica e programmazione. 
