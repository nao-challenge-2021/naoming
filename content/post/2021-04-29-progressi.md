---
title: Grandi progressi 
date: 2021-04-29
---
{{< youtube id="symNtvKDO24" >}}

[Questo è il nostro progetto caricato su gitlab.com]
(https://gitlab.com/nao-challenge-2021/NAOmingEv3dev)

Stiamo lavorando per la seconda prova, vogliamo utilizzare un sensore esterno da implementare al nostro robot NAO.
Stiamo utilizzando Visual Studio Code per compilare con MicroPython for EV3 un programma che interfacci Mindstorms EV3 con NAO.
Il link sopra apre il progetto sorgente a cui stiamo lavorando.
