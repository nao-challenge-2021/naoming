---
title: Problemi e come affrontarli
date: 2021-05-01
---

![Foto di noi](/immagine1.jpg)

## Come abbiamo affrontato la pandemia

Durante questi lunghi mesi di preparazione alla NaoChallenge siamo stati costretti a far fronte a vari problemi. Tra i più insidiosi ricordiamo  sicuramente quello legato alla pandemia, che ci ha impedito di dedicarci a questo progetto, interrompendo il nostro lavoro. Nonostante ciò, al ritorno a scuola in presenza eravamo più determinati che mai e in un certo senso si può dire che questa lunga pausa ci abbia fatto bene, in modo da riorganizzare il lavoro e pensare a nuove idee. Proprio per questo motivo lo scorso inverno è stato molto intenso dal punto di vista lavorativo, inoltre le attività di gruppo hanno aiutato a formare un gruppo ancora più unito e armonioso.
