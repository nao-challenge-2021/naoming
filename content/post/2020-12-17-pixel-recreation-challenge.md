---
title: Pixel Recreation Challenge
date: 2020-12-17
---
{{< gviewer 19MwKQkJYj0dgYpJ4hn-jZUqUEnFcFvyV8ogqKpt6pW4 >}}

Queste sono delle simpatiche pixel art.

Siamo la squadra Naoming del liceo scientifico Rainerum di Bolzano. Lo scorso anno scolastico abbiamo preso parte alla NAO Challenge grazie all'aiuto del nostro professore di informatica Julian Sanin.
Dopo aver analizzato la struttura della competizione, abbiamo pensato di sviluppare un argomento inerente alla Divina Commedia, col supporto della professoressa di italiano.
Abbiamo cominciato dividendoci in sottogruppi, ognuno col proprio compito da svolgere. A seguito di attente ricerche abbiamo iniziato a sviluppare delle domande inerenti alla Divina Commedia e al periodo storico di Dante, con lo scopo di inserirle nel robot. Infatti, a seguito delle domande specifiche, il robot sarebbe in grado di rispondere in maniera completa ed esaustiva.
Abbiamo proseguito sviluppando un programma a blocchi utilizzando Choregraphe.

Sfortunatamente nel mese di Marzo scorso abbiamo dovuto interrompere il nostro lavoro a causa della pandemia di SARS-CoV-2.
Al nostro ritorno in aula ci siamo trovati spaesati e abbiamo avuto difficoltà nel ricordare con precisione cosa ancora mancasse per completare le nostre tasks.
